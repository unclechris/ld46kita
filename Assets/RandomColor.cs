using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColor : MonoBehaviour
{
	private SpriteRenderer Spr;
	// Start is called before the first frame update
	void Awake()
	{
		if (Spr == null)
		{
			Spr = GetComponent<SpriteRenderer>();
			Color RGBColor = Spr.color;
			float H;
			float S;
			float V;
			Color.RGBToHSV(RGBColor, out H, out S, out V);
			H = Random.Range(0f, 1f);
			RGBColor = Color.HSVToRGB(H, S, V);
			Spr.color = RGBColor;
		}
	}

}
