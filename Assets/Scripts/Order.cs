using System.Collections.Generic;

public class Order
{
	public int Base = -1;
	public int Sauce = -1;
	public int Cheese = -1;
	public List<int> Toppings = new List<int>();
}
