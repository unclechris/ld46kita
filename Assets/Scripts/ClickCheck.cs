using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickCheck : MonoBehaviour
{
	private SpriteRenderer Spr;
	private Animator ani;
	public Color clickColor;
	private Color unclickColor;
	public bool buttonEnabled;
	
	private void Start()
	{
		if (Spr==null)
		{
			Spr = GetComponent<SpriteRenderer>();
			unclickColor = Spr.color;
		}		
		if (ani==null)
		{
			ani = GetComponent<Animator>();
		}
		if (!buttonEnabled)
		{
			Spr.color = Color.grey;
		}
	}
	private void Update()
	{
		if (buttonEnabled)
		{
			Spr.color = unclickColor;
		}
		else
		{

			Spr.color = Color.grey;
		}
	}
	
	private void OnMouseDown()
	{
		if (buttonEnabled)
		{
			Debug.Log(Spr.transform.name);
			Spr.color = clickColor;

			if (ani != null)
			{ ani.SetTrigger("Click"); }
			AudioManager.Instance.Play("Click");
			GameManager.Instance.ClickedButton(Spr.transform.name);
		}
	}
	private void OnMouseUp()
	{
		if (buttonEnabled)
		{
			Spr.color = unclickColor;
		}
	}
	private void OnMouseExit()
	{
		if (buttonEnabled)
		{
			Spr.color = unclickColor;
		}
	}
}
