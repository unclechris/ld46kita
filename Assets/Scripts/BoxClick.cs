using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxClick : MonoBehaviour
{
	public SpriteRenderer Spr;
	public Color clickColor;
	public Color unclickColor;
	public Transform boxITPoint;
	private void Start()
	{
		if (Spr == null)
		{
			Spr = GetComponent<SpriteRenderer>();
			unclickColor = Spr.color;
		}
	}
	private void OnMouseDown()
	{
			Spr.color = clickColor;
	}
	private void OnMouseUp()
	{
			Spr.color = unclickColor;
	}
	private void OnMouseExit()
	{
			Spr.color = unclickColor;
	}
}
