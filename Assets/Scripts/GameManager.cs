using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum PizzaLayers
{
	Base, Sauce, Cheese, Topping
}


public class GameManager : MonoBehaviour
{
	public static GameManager Instance { get; private set; }
	public bool CustomerMoving { get; private set; }
	public Transform PizzaLocation { get; private set; }
	public int Wave { get; private set; }
	public GameObject myOrderPizza { get; private set; }
	public GameObject myPizza { get; private set; }
	

	private const int NUMCUSTOMERSPERWAVE = 10;
	public GameObject FloatingTextPreFab;
	public GameObject Till;
	private TextMeshPro TillScreen;
	private GameObject myFloatingText;
	public Transform[] ITWalkPoints;
	public Transform ITHome;
	public GameObject ITGuyPrefab;
	private GameObject ITGuy;
	public GameObject WorkClouds;
	public GameObject BlueSmoke;
	private GameObject myWork;
	public GameObject PizzaPrefab;
	public GameObject CustomerPrefab;
	private GameObject myCustomer;
	public Transform[] CustomerWalkPoints;
	public Transform OrderDisplay;
	public Order currentRequestedOrder;
	public Transform PizzaBuildLocation;
	public Transform PizzaDeliverPoint;
	public Transform PizzaTrashPoint;
	public Transform PizzaITPoint;
	public GameObject[] Bases;
	public GameObject[] Sauces;
	public GameObject[] Cheeses;
	public GameObject[] Toppings;
	public Order workingOrder;
	public float ITGuySpeed = 8.0f;
	public float CustomerSpeed = 8.0f;
	private float ITGuyWaitTime;
	private int CurrentOrder;
	public float ITGuyStartWaitTime;
	public bool ITGuyEnabled = false;
	public int PointToFix = -1;
	public Vector3 ITGuyWalkToPosition;
	public Vector3 CustomerWalkToPosition;
	private int numCustomers;	
	private bool PizzaMoving;
	private float PizzaSpeed = 5.0f;
	private int Earnings = 0;
	private int ITHappiness;
	private int MachineHealth = 100;
	private bool GameOver = false;
	private bool PlayWarning;

	public void ClickedButton(string name)
	{
		if (GameOver)
		{ return; }

		Debug.Log(name + " button clicked.");
		if (name == "DeliverButton")
		{
			CustomerMoving = true;
			DeliverPizza();
			NewPizza();
		}
		else
		if (name=="TrashButton")
		{
			ThrowAwayPizza();
			NewPizza();
		}

		if (name == "GiveITButton")
		{
			GiveITPizza();
			NewPizza();
		}		
		string buttonNumber = new String(name.Where(Char.IsDigit).ToArray());
		int number = 0;
		if (buttonNumber.Length > 0)
		{
			number = Convert.ToInt32(buttonNumber);
		}
		Debug.Log(number);
		if (name.ToUpper().Contains("CRUST"))
		{
			// Have we added a Base already?
			if (workingOrder.Base == -1)
			{
				if (Bases.Length >= number && number >= 0)
				{
					var go = Instantiate(Bases[number], myPizza.transform.position, Quaternion.identity, myPizza.transform);
					go.GetComponent<SpriteRenderer>().sortingOrder = CurrentOrder++;
					workingOrder.Base = number;
				}
			}
			else
			{
				AudioManager.Instance.Play("WrongOrder");
				BreakMachine(-30);
			}
		}

		if (name.ToUpper().Contains("SAUCE"))
		{
			// Have we added a Base already?
			if (workingOrder.Sauce == -1 && workingOrder.Base != -1)
			{
				if (Sauces.Length >= number && number >= 0)
				{
					var go = Instantiate(Sauces[number], myPizza.transform.position, Quaternion.identity, myPizza.transform);
					go.GetComponent<SpriteRenderer>().sortingOrder = CurrentOrder++;
					workingOrder.Sauce = number;
				}
			}
			else
			{
				AudioManager.Instance.Play("WrongOrder");
				BreakMachine(-30);
			}
		}

		if (name.ToUpper().Contains("CHEESE"))
		{
			// Have we added a Base already?
			if (workingOrder.Cheese == -1 && (workingOrder.Base != -1 && workingOrder.Sauce == currentRequestedOrder.Sauce))
			{
				if (Cheeses.Length >= number && number >= 0)
				{
					var go=Instantiate(Cheeses[number], myPizza.transform.position, Quaternion.identity, myPizza.transform);
					go.GetComponent<SpriteRenderer>().sortingOrder = CurrentOrder++;
					workingOrder.Cheese = number;
				}
			}
			else
			{
				AudioManager.Instance.Play("WrongOrder");
			}
		}

		if (name.ToUpper().Contains("TOPPING"))
		{
			if ((workingOrder.Base != -1 && workingOrder.Sauce == currentRequestedOrder.Sauce))
			{
				if (Toppings.Length >= number && number >= 0 && !workingOrder.Toppings.Contains(number))
				{
					var go = Instantiate(Toppings[number], myPizza.transform.position, Quaternion.identity, myPizza.transform);
					go.GetComponent<SpriteRenderer>().sortingOrder = CurrentOrder++;
					workingOrder.Toppings.Add(number);
				}
			}
			else
			{
				AudioManager.Instance.Play("WrongOrder");
				BreakMachine(-30);
			}
		}

	}
	IEnumerator WaitForTime(float sec)
	{
		yield return new WaitForSecondsRealtime(sec);
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
	private void SetMachineHealth(int x)
	{
		MachineHealth = x;
	}

	private void BreakMachine(int v)
	{
		MachineHealth += v;
		if (MachineHealth<=0)
		{
			AudioManager.Instance.Play("GAMEOVER");
			if (FloatingTextPreFab != null)
			{
				var go = Instantiate(FloatingTextPreFab, Vector3.zero, Quaternion.identity);
				var tmp = go.GetComponent<TextMeshPro>();
				tmp.text = "GAME OVER";
			}
			GameOver = true;
			StartCoroutine(WaitForTime(3.0f));
			return;
		}
		if (MachineHealth < 33)
		{
			AudioManager.Instance.Play("BlueSmoke");
			PointToFix = UnityEngine.Random.Range(0, ITWalkPoints.Length);
			Vector3 SmokeHatch = ITWalkPoints[PointToFix].position;
			var go = Instantiate(BlueSmoke, SmokeHatch, Quaternion.identity);
			return;
		}
		if (MachineHealth != 0 && !PlayWarning)
		{
			AudioManager.Instance.Play("YouNeedHelp");
			PlayWarning = true;
			return;
		}


	}

	private void NewPizza()
	{
		if (myPizza !=null)
		{
			Destroy(myPizza);
		}
		myPizza = Instantiate(PizzaPrefab, PizzaBuildLocation.position, Quaternion.identity);
		myPizza.GetComponent<SpriteRenderer>().sortingOrder =2;
		workingOrder = new Order();
		CurrentOrder = 3;
	}
	private void ThrowAwayPizza()
	{
		PizzaMoving = true;
		PizzaLocation = PizzaTrashPoint;
		UpdateEarnings(-500);
	}

	private void SetEarnings(int v)
	{
		Earnings = v;
		UpdateEarnings(0);
	}

	private void UpdateEarnings(int v)
	{
		Earnings += v;		
		if (TillScreen != null)
		{
			TillScreen.text = string.Format("{0:c}", Convert.ToDecimal(Earnings / 100));
		}
	}

	private void GiveITPizza()
	{
		PizzaMoving = true;
		PizzaSpeed = ITGuySpeed;
		PizzaLocation = PizzaITPoint;
		UpdateEarnings(-1000);
	}
	
	private void DeliverPizza()
	{
		PizzaLocation = PizzaDeliverPoint;
		PizzaMoving = true;
		AudioManager.Instance.Play("Suck");	
		if (workingOrder.Base == currentRequestedOrder.Base &&
			workingOrder.Sauce == currentRequestedOrder.Sauce &&
			workingOrder.Cheese == currentRequestedOrder.Cheese &&
			!workingOrder.Toppings.Except(currentRequestedOrder.Toppings).Any() &&
			!currentRequestedOrder.Toppings.Except(workingOrder.Toppings).Any() )
		{
			Debug.Log(workingOrder);
			Debug.Log(currentRequestedOrder);
			UpdateEarnings(2500);
			AudioManager.Instance.Play("Yeah");
		}
		else
		{
			AudioManager.Instance.Play("Yuck");
		}
		
	}

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
			Destroy(gameObject);
	}
	private void Start()
	{
		Wave = 1;
		ITHappiness = 100;
		PointToFix = -1;
		GameOver = false;
		if (Till!=null)
		{
			TillScreen = Till.GetComponent<TextMeshPro>();
		}
		SetEarnings(0);
		NewCustomer();
		NewPizza();
	}

	public void ShowITGuyFloatingText(string TextToShow)
	{
		if (FloatingTextPreFab != null)
		{
			var go = Instantiate(FloatingTextPreFab, ITGuy.transform.position,Quaternion.identity,ITGuy.transform);
			go.GetComponent<TextMeshPro>().text = TextToShow;
		}
	}
	private void ITGuyUpdate()
	{
		if (ITGuyEnabled)
		{
			if (ITGuyPrefab != null && ITGuy == null)
			{
				ITGuy = Instantiate(ITGuyPrefab, ITHome.position, Quaternion.identity);
				ITGuyWaitTime = ITGuyStartWaitTime;
				if (PointToFix ==-1)
				{
					PointToFix = UnityEngine.Random.Range(0, ITWalkPoints.Length);
				}
				ITGuyWalkToPosition = ITWalkPoints[PointToFix].position;
			}
			if (ITGuy != null)
			{
				ITGuy.transform.position = Vector2.MoveTowards(ITGuy.transform.position, ITGuyWalkToPosition, ITGuySpeed * Time.deltaTime);
				if (Vector2.Distance(ITGuy.transform.position, ITGuyWalkToPosition) < 0.2f)
				{
					if (ITGuyWaitTime <= 0)
					{
						if (PointToFix != -1)
						{
							//If we are at our spot - then choose to move back to home location
							PointToFix = -1;
							ITGuyWalkToPosition = ITHome.position;
						}
						else
						{
							ITGuyEnabled = false;
							//Destroy(ITGuy);
							Destroy(myWork);
						}
					}
					else
					{
						ITGuyWaitTime -= Time.deltaTime;

						if (myWork == null)

						{
							myWork = Instantiate(WorkClouds, ITGuy.transform.position, Quaternion.identity);
							AudioManager.Instance.Play("ToolSounds");
							SetMachineHealth(MachineHealth+ITHappiness);
						}
					}

				}
			}
		}
	}
	private void NewCustomer()
	{
		if (CustomerPrefab != null && myCustomer == null)
		{
			myCustomer = Instantiate(CustomerPrefab, CustomerWalkPoints[0].position, Quaternion.identity);
			CustomerWalkToPosition = CustomerWalkPoints[1].position;
			CustomerMoving = true;
			AudioManager.Instance.RandomPitch("PlaceOrder", 1.25f, 2.0f);
			currentRequestedOrder = new Order();
			CurrentOrder = 200;
			myOrderPizza = Instantiate(PizzaPrefab, OrderDisplay.position, Quaternion.identity, OrderDisplay);
			myOrderPizza.GetComponent<SpriteRenderer>().sortingOrder = CurrentOrder++;
			currentRequestedOrder.Base = UnityEngine.Random.Range(0, Bases.Length);
			var OrderBase = Instantiate(Bases[currentRequestedOrder.Base], OrderDisplay.position, Quaternion.identity, myOrderPizza.transform);
			OrderBase.GetComponent<SpriteRenderer>().sortingOrder = CurrentOrder++;
			currentRequestedOrder.Sauce = UnityEngine.Random.Range(0, Sauces.Length);
			var OrderSauce = Instantiate(Sauces[currentRequestedOrder.Sauce], OrderDisplay.position, Quaternion.identity, myOrderPizza.transform);
			OrderSauce.GetComponent<SpriteRenderer>().sortingOrder = CurrentOrder++;
			currentRequestedOrder.Cheese = UnityEngine.Random.Range(0, Cheeses.Length);
			var OrderCheese = Instantiate(Cheeses[currentRequestedOrder.Cheese], OrderDisplay.position, Quaternion.identity, myOrderPizza.transform);
			OrderCheese.GetComponent<SpriteRenderer>().sortingOrder = CurrentOrder++;
			int NumToppings = 0;
			while (UnityEngine.Random.Range(0f, 1f) < .5f )
			{
				NumToppings=1;
			}
			if (NumToppings>0)
			{
				// Add new toppings
				int curTopping = -1;
				while (curTopping == -1)
				{
					curTopping = UnityEngine.Random.Range(0, Toppings.Length);
					if (currentRequestedOrder.Toppings.Contains(curTopping))
					{
						curTopping = -1;
					}
				}
				currentRequestedOrder.Toppings.Add(curTopping);
				var go = Instantiate(Toppings[curTopping], OrderDisplay.position, Quaternion.identity, myOrderPizza.transform);
				go.GetComponent<SpriteRenderer>().sortingOrder = CurrentOrder++;
			}
		}
	}
	private void CustomerUpdate()
	{
		if (CustomerMoving)
		{
			myCustomer.transform.position = Vector2.MoveTowards(myCustomer.transform.position, CustomerWalkToPosition, CustomerSpeed * Time.deltaTime);
			if (Vector2.Distance(myCustomer.transform.position, CustomerWalkToPosition) < 0.2f)
			{
				if (CustomerWalkToPosition == CustomerWalkPoints[1].position)
				{
					CustomerMoving = false;
					CustomerWalkToPosition = CustomerWalkPoints[2].position;
					PlaceOrder();
				}
				else if
					(CustomerWalkToPosition == CustomerWalkPoints[2].position)
				{
					CustomerMoving = false;
					Destroy(myCustomer);
					Destroy(myOrderPizza);
					myCustomer = null;
					if (numCustomers== NUMCUSTOMERSPERWAVE)
					{
						numCustomers = 0;
						Wave++;
					}
					NewCustomer();
				}
			}
		}
	}
	private void PizzaUpdate()
	{
		if (PizzaMoving)
		{
			myPizza.transform.position = Vector2.MoveTowards(myPizza.transform.position, PizzaLocation.position, PizzaSpeed * Time.deltaTime);
			if (Vector2.Distance(myPizza.transform.position, PizzaLocation.position) < 0.2f)
			{
				if (PizzaLocation == PizzaTrashPoint)
				{
					PizzaMoving = false;
					Destroy(myPizza);
					NewPizza();
				}
				else if (PizzaLocation == PizzaDeliverPoint)
				{
					PizzaMoving = false;
					Destroy(myPizza);
					NewPizza();
				}
			}
		}
	}

	private void PlaceOrder()
	{
		AudioManager.Instance.Play("PlaceOrder");
	}
	private void Update()
	{
		if (ITGuyEnabled)
		{
		ITGuyUpdate();
		}		
		CustomerUpdate();
		if (PizzaMoving)
		{
			PizzaUpdate();
		}
	}
}
