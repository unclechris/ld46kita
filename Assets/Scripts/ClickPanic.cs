using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickPanic : MonoBehaviour
{
	public SpriteRenderer Spr;
	public Color clickColor;
	public Color unclickColor;
	public Transform spawnPoint;
	public GameObject mySprite;
	public bool buttonEnabled;

	private void Start()
	{
		if (Spr==null)
		{
			Spr = GetComponent<SpriteRenderer>();
			unclickColor = Spr.color;
		}		
	}

	private void OnMouseDown()
	{
		if (buttonEnabled && !GameManager.Instance.ITGuyEnabled)
		{
			Spr.color = clickColor;
			GameManager.Instance.ITGuyEnabled = true;
		}
		else
		{
			GameManager.Instance.ShowITGuyFloatingText("I am working as fast as I can!");
			AudioManager.Instance.Play("panic");
		}

	}
	private void OnMouseUp()
	{
		if (buttonEnabled)
		{
			Spr.color = unclickColor;
		}
	}
	private void OnMouseExit()
	{
		if (buttonEnabled)
		{
			Spr.color = unclickColor;
		}
	}
}
