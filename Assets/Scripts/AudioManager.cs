using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	public static AudioManager Instance { get; private set; }

	public Sound[] sounds;
	// Start is called before the first frame update
	void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
		{
			Destroy(gameObject);
			return;
		}
		DontDestroyOnLoad(gameObject);

		foreach (Sound s in sounds)
		{
			s.source = gameObject.AddComponent<AudioSource>();
			s.source.clip = s.clip;
			s.source.pitch = s.pitch;
			s.source.volume = s.volume;
			s.source.loop = s.Loop;
		}
	}
	private void Start()
	{
		Play("Theme");
	}
	public void Play(string name)
	{
		Sound s = Array.Find(sounds, sound => sound.name == name);
		if (s == null)
		{
			Debug.LogWarning("Sound: " + name + " not found!");
			return;
		}
		s.source.Play();
	}
	public void RandomPitch(string name, float minPitch, float maxPitch)
	{
		Sound s = Array.Find(sounds, sound => sound.name == name);
		if (s == null)
		{
			s.pitch = UnityEngine.Random.Range(minPitch, maxPitch);
		}
	}
}
